## requirements
- node
- python 3.x

## running project
- run cmd: cd project_folder
- run cmd: python server.py
- open in browser: http://localhost:8888/

## CHIP-8 features
- 8 Kb memory
- 64x32 display resolution
- 60Hz timers