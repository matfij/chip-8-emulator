class Speaker {

    constructor() {
        const AUDIO_CONTEXT = window.AudioContext || window.webkitAudioContext;
        this.audioContext = new AudioContext();

        // connected audio node (volume control)
        this.gain = this.audioContext.createGain();
        this.finish = this.audioContext.destination;
        this.gain.connect(this.finish);
    }

    mute() {
        this.gain.setValueAtTime(0, this.audioContext.currentTime);
    }

    unmute() {
        this.gain.setValueAtTime(1, this.audioContext.currentTime);
    }

    play(freq) {
        if (this.audioContext && !this.oscillator) {
            this.oscillator = this.audioContext.createOscillator();
            this.oscillator.frequency.setValueAtTime(freq || 440, this.audioContext.currentTime);
            this.oscillator.type = 'square';

            this.oscillator.connect(this.gain);
            this.oscillator.start();
        }
    }

    stop() {
        if (this.oscillator) {
            this.oscillator.stop();
            this.oscillator.disconnect();
            this.oscillator = null;
        }
    }
}

export default Speaker;
