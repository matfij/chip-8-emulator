class Renderer {
    
    constructor(scale) {
        // display resolution
        this.cols = 64;
        this.rows = 32;

        // display scalling
        this.scale = scale;
        this.canvas = document.querySelector('canvas');
        this.context = this.canvas.getContext('2d');
        this.canvas.width = this.cols * this.scale;
        this.canvas.height = this.rows * this.scale;

        // display initialization
        this.display = new Array(this.cols * this.rows);
    }

    setPixel(x, y) {
        // handle display wrapping
        if (x > this.cols) {
            x -= this.cols;
        } else if (x < 0) {
            x += this.cols;
        }

        if (y > this.rows) {
            y += this.rows;
        } else if (y < 0) {
            y -= this.rows
        }

        let location = x + (y * this.cols);

        // XOR sprit to the display - pixel value toggle
        this.display[location] ^= 1;

        // return performed action - true = erased, false - not erased
        return !this.display[location];
    }

    clear() {
        this.display = new Array(this.cols * this.rows);
    }

    render() {
        // clear display at 60Hz rate
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        // draw pixels
        for (let ind = 0; ind < this.cols * this.rows; ind++) {
            // get current pixel position
            let x = (ind%this.cols) * this.scale;
            let y = Math.floor(ind/this.cols) * this.scale;
            // draw pixel
            if (this.display[ind]) {
                this.context.fillStyle = '#f0f';
                this.context.fillRect(x, y, this.scale, this.scale)
            }
        }
    }
}

export default Renderer;
