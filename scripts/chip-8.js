import Renderer from './renderer.js';
import Keyboard from './keyboard.js';
import Speaker from './speaker.js';
import CPU from './cpu.js';

const renderer =  new Renderer(10);
const keyboard = new Keyboard();
const speaker = new Speaker();
const cpu = new CPU(renderer, keyboard, speaker);

let loop;
let clockFrequency = 60, clockInterval, startTime, currentTime, previousTime, elapsedTime;


function init() {
    clockInterval = 1000 / clockFrequency;
    previousTime = Date.now();
    startTime = previousTime;

    cpu.loadSprites();
    cpu.loadRom('MAZE');

    loop = requestAnimationFrame(step);
}


function step() {
    currentTime = Date.now();
    elapsedTime = currentTime - previousTime;

    if (elapsedTime > clockInterval) {
        cpu.cycle();
    }

    loop = requestAnimationFrame(step);
}


init();
